import React, { useState, useEffect } from 'react'
import Header from './components/Header'
import MessageList from './components/MessageList'
import Preloader from './components/Preloader'

const Chat = (props) => {
  const [messages, setMessages] = useState([])
  const [isLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    const fetchData = async () => {
      const res = await fetch(props.url)
      setMessages(await res.json())
      setIsLoaded(true)
    }

    fetchData()
  }, [])

  if (isLoaded) {
    return (
      <div className="chat">
        <Header messages={messages} />
        <MessageList messages={{ messages, setMessages }} />
      </div>
    )
  } else {
    return <Preloader />
  }
}

export default Chat
