import React, { useState, useEffect } from 'react'
import moment from 'moment'

const Header = (props) => {
  const [data, setData] = useState({})
  const { messages } = props

  useEffect(() => {
    if (messages) {
      const users = new Set()
      messages.map((e) => users.add(e.userId))

      const lastMessage = messages[messages.length - 1]
      if (lastMessage.isOwnMessage) {
        const time = `Today, ${lastMessage.createdAt}`

        setData({
          users: users.size,
          messages: messages.length,
          lastMessageDate: time,
        })
      } else {
        const time = moment(lastMessage.createdAt).format('DD.MM.YYYY HH:mm')

        setData({
          users: users.size,
          messages: messages.length,
          lastMessageDate: time,
        })
      }
    }
  }, [messages])

  return (
    <header className="header">
      <h2 className="header-title">My Chatie</h2>
      <div className="header-meta">
        <div>
          <div className="header-users-count">
            <h4>{data.users}</h4>
          </div>{' '}
          users
        </div>
        <div>
          <div className="header-messages-count">
            <h4>{data.messages}</h4>
          </div>{' '}
          messages
        </div>
        <div>
          Last Message
          <div className="header-last-message-date">
            <h4>{data.lastMessageDate}</h4>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header
