import React, { useRef, useEffect, useState } from 'react'
import Message from './Message'
import OwnMessage from './OwnMessage'
import MessageInput from './MessageInput'
import { v4 as uuidv4 } from 'uuid'

const MessageList = (props) => {
  const messagesEndRef = useRef(null)
  const { messages, setMessages } = props.messages

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: 'smooth' })
  }
  useEffect(scrollToBottom, [messages])

  return (
    <div>
      <div className="message-list">
        {messages.map((e) =>
          e.isOwnMessage ? (
            <OwnMessage
              key={e.id}
              message={e}
              msg={{ messages, setMessages }}
            />
          ) : (
            <Message key={e.id} message={e} />
          )
        )}
        <div ref={messagesEndRef} />
      </div>
      <MessageInput msg={{ messages, setMessages }} />
    </div>
  )
}

export default MessageList
