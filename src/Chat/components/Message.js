import React from 'react'
import moment from 'moment'

const Message = (props) => {
  const { text, createdAt, user, avatar } = props.message

  const likeHandler = (e) => {
    const el = e.target.parentElement

    if (el.className == 'message-like') {
      el.className = 'message-liked'

      el.innerHTML = `<i class="fas fa-heart"></i>`
    } else if (el.className == 'message-liked') {
      el.className = 'message-like'
      el.innerHTML = `<i class="far fa-heart"></i>`
    }

    console.log(el.classList)
  }

  return (
    <div className="message">
      <div className="message-user-avatar">
        <img src={avatar} alt="" />
      </div>
      <div className="message-content">
        <div className="message-user-name">{user}</div>
        <div className="message-text">{text}</div>
        <div className="message-time">{moment(createdAt).format('HH:mm')}</div>
        <div onClick={likeHandler} className="message-like">
          <i className="far fa-heart"></i>
        </div>
      </div>
    </div>
  )
}

export default Message
