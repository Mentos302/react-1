import React from 'react'
import { Spinner } from 'reactstrap'

const Preloader = () => {
  return (
    <div className="preloader">
      <Spinner style={{ width: '3rem', height: '3rem' }} children=" " />{' '}
      <h4>Please wait, loading</h4>
    </div>
  )
}

export default Preloader
