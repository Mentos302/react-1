import React, { useState } from 'react'
import moment from 'moment'
import { v4 as uuidv4 } from 'uuid'

const MessageInput = (props) => {
  const [message, setMessage] = useState('')
  const { messages, setMessages } = props.msg

  const submitHandler = () => {
    setMessage('')
    setMessages(
      messages.concat({
        text: message,
        isOwnMessage: true,
        createdAt: moment().format('HH:mm'),
        id: uuidv4(),
      })
    )
  }

  return (
    <div className="message-input">
      <input
        onChange={(e) => setMessage(e.target.value)}
        className="message-input-text"
        type="text"
        value={message}
      />
      <button className="message-input-button" onClick={submitHandler}>
        Send
      </button>
    </div>
  )
}

export default MessageInput
