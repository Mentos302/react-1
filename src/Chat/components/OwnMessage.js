import React, { useState } from 'react'

const OwnMessage = (props) => {
  const { text, createdAt, id } = props.message
  const { messages, setMessages } = props.msg

  const deleteHandler = (e) => {
    const el = e.target.parentElement.parentElement

    setMessages(messages.filter((e) => e.id != id))
  }

  const editHandler = (e) => {
    const el = e.target.parentElement.parentElement.childNodes[0]

    if (el.className != `ownMessageContent`) {
      el.innerHTML = `<input class="editingInput" value='${el.innerText}' type="text" />`

      let editedMessage = ''

      const input = document.querySelector('.editingInput')
      if (input) {
        input.addEventListener('input', (e) => {
          editedMessage = e.target.value
        })
        input.addEventListener('blur', () => {
          el.innerHTML = editedMessage
        })
      }
    }
  }

  return (
    <div className="own-message">
      <div className="ownMessageContent">
        <div className="message-text">{text}</div>
        <div className="message-time">{createdAt}</div>
        <div className="message-like">
          <i className="far fa-heart"></i>
        </div>
        <div onClick={editHandler} className="message-edit">
          <i className="fas fa-pencil-alt"></i>
        </div>
        <div onClick={deleteHandler} className="message-delete">
          <i className="fas fa-times"></i>
        </div>
      </div>
    </div>
  )
}

export default OwnMessage
