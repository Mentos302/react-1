import './App.css'
import Chat from './Chat/Chat'

function App() {
  return (
    <div className="App">
      <div className="AppLogo">
        <img src="https://upload.wikimedia.org/wikipedia/commons/8/88/SILVAIR_logotype.png" />
      </div>
      <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
    </div>
  )
}

export default App
